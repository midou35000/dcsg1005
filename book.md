# Cloud Computing

## Cloud Computing

##### Cloud Computing

  - SaaS

  - PaaS

  - IaaS

Let’s see what the *public clouds* AWS, Azure and Google has to offer

##### Our Key Aspect

  - Self-service\!

  - It’s dynamic\!

  - (A measured service)

## Security and Privacy

##### Security and Privacy

  - Privacy: storing data on someone else’s computers

  - All issues related to outsourcing (possibly loosing control)

  - Otherwise mostly same issues as on-premise
    (patch/update,backups,access control,logs,monitoring)

Remember how simple it is to encrypt (and decrypt) a file with a cross
platform tool with an open implementation of an open widely used crypto
algorithm ([7-zip](https://www.7-zip.org) or
[OpenSSL](https://www.openssl.org) with AES-256):

    $ echo "my secret info" > confidential.txt
    #
    # 7-zip
    #
    $ dpkg -S $(which 7z)
    p7zip-full: /usr/bin/7z
    $ 7z a -p confidential.txt.7z confidential.txt # encrypt
    $ 7z e confidential.txt.7z                     # decrypt
    #
    # OpenSSL
    #
    $ dpkg -S $(which openssl)
    openssl: /usr/bin/openssl
    $ openssl enc -aes-256-cbc -a -pbkdf2 \
       -in confidential.txt -out confidential.txt.enc    # encrypt
    $ openssl enc -aes-256-cbc -a -pbkdf2 \
       -d -in confidential.txt.enc -out confidential.txt # decrypt

## OpenStack

##### Cloud Computing

  - [OpenStack software](https://www.openstack.org/software)

## Review questions and problems

1.  PaaS is where
    
    1.  you create virtual machines, block storage and basic networking
        components.
    
    2.  you create applications and use tools, libraries and services
        from the cloud provider.
    
    3.  you use applications that are hosted (are running) in the cloud.
    
    4.  you use tools that only can be activated as a service.

2.  In which time period did AWS EC2, Google App Engine, Microsoft Azure
    and OpenStack appear?
    
    1.  1996-2000.
    
    2.  2001-2005.
    
    3.  2006-2010.
    
    4.  2011-2015.

3.  What do you consider as a major *privacy* concern/threat when you
    use a public cloud? Give an example including a technical measure (a
    technical solution) to prevent it.

4.  What is a *security group* in OpenStack? What do you use it for?

5.  In pseudo code, write an ordered list (reflecting the sequence you
    would run) of `openstack` (including all command line arguments) to
    set up your own router, network and Ubuntu server in SkyHiGh in such
    a way that you can reach it with ssh from other hosts in the
    NTNU-network (similar to what we have done in the lab).

## Lab tutorials

1.  You will be using NTNUs private OpenStack cloud
    [SkyHiGh](https://www.ntnu.no/wiki/display/skyhigh). OpenStack is an
    cloud solution composed of many sub projects. SkyHiGh is accessible
    as long as you are in the NTNU network (use VPN if you need access
    from outside).
    
    Each OpenStack project has its own client, but these individual
    clients are now deprecated in favor of a common `openstack` client
    to standardize and simplify the interface :
    
    > The following individual clients are deprecated in favor of a
    > common client. Instead of installing and learning all these
    > clients, we recommend installing and using the OpenStack client.
    
    You are expected to solve this exercise using the [Openstack
    Command-line
    Client](https://docs.openstack.org/python-openstackclient/latest/)
    
    (note: in some special cases you might have to still use to old
    commands if the `openstack` command does not support the operation)
    
    (btw for tab completion of subcommands do something like  
    `openstack complete >> ~/.bashrc`)
    
    You will be provided with all commands you need to execute most of
    the time, but make sure you understand what is happening (not just
    copy and paste without thinking). This is the foundation for scaling
    up operations later in the course.
    
    *Parts of commands that are written in UPPERCASE are meant to be
    replaced by you*.
    
    Tip: View your infrastructure in  
    <https://skyhigh.iik.ntnu.no/horizon/project/network_topology/>  
    as we go along.
    
      - To access to lab from outside networks, you need to be connected
        by VPN to the NTNU network, see [Install
        VPN](https://innsida.ntnu.no/wiki/-/wiki/English/Install+VPN)
    
      - For RDP on Windows, use built-in Remote Desktop Connection
        (`mstsc`), if you are using Windows 8 or newer, consider
        applying this fix (you probably have to apply only the first
        one) first:  
        <https://polarclouds.co.uk/fixing-remote-desktop-annoyances/>
        
        On Mac, get Microsoft Remote Desktop connection from the App
        store.
        
        On Linux, use something like:  
        `xfreerdp /u:Admin +clipboard /w:1366 /h:768 /v:SERVER_IP`
    
    <!-- end list -->
    
    1.  Decide on where you want your work environment to be: on your
        laptop or on a server (e.g. `login.stud.ntnu.no`). If you use
        your laptop, you need to install the
        [python-openstackclient](https://github.com/openstack/python-openstackclient).
        If you ssh to a server you are strongly advised to use tmux (or
        screen if you prefer) session. If you just want to get started
        fast, I recommend you ssh to `login.stud.ntnu.no`.
        
        If you use Windows 10, an SSH client is included. Should the
        `ssh` command not be present on your system, OpenSSH can be
        installed with [Installation of OpenSSH For Windows Server 2019
        and
        Windows 10](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse).
        On older versions of Windows or if you run into [one of the
        still unresolved issues of the OpenSSH
        packages](https://github.com/PowerShell/Win32-OpenSSH/issues),
        you can use
        [PuTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/).
    
    2.  Familiarize yourself with the most widely used OpenStack CLI
        commands:
        
            openstack -h | less
        
        Set environment variables using the OpenStack RC file:
        
        1.  Login to SkyHiGh GUI, “API Access”, “Download OpenStack RC
            file” (Identity API v3)
        
        2.  Copy this file to your work environment (e.g.  
            `scp PRIV_user-openrc.sh USER@login.stud.ntnu.no:`)
        
        3.  Source the file: `. PRIV_user-openrc.sh`
        
        4.  Check that you now can run commands without problems, e.g.  
            `openstack keypair list`
        
        Alternatively, if you don’t want to have your cleartext password
        as a environment variable, see [Authenticating the CLI-client
        using
        tokens](https://www.ntnu.no/wiki/display/skyhigh/Authenticating+the+CLI-client+using+tokens).
    
    3.  Create a key pair (e.g. use your username as key name). Take
        good care of the private key\!
        
            openstack keypair create KEY_NAME > KEY_NAME.pem
    
    4.  Create a network.
        
            openstack network create net1
            # old deprecated way:
            neutron net-create net1
    
    5.  Create a subnet `192.168.1XY.0/24` where `XY` is your choice.
        
            openstack subnet create subnet1 --network net1 \
             --subnet-range 192.168.1XY.0/24
            # old deprecated way:
            neutron subnet-create net1 192.168.1XY.0/24 --name subnet1
    
    6.  Create a router.
        
            openstack router create router1
            # old deprecated way:
            neutron router-create router1
    
    7.  Set the router’s gateway to be in `ntnu-internal`.
        
            openstack router set router1 --external-gateway ntnu-internal
            # old deprecated way:
            neutron router-gateway-set router1 ntnu-internal
    
    8.  Add an interface on the router to your subnet.
        
            openstack router add subnet router1 subnet1
            # old deprecated way:
            neutron router-interface-add router1 subnet1
        
        You now have a network where you can boot your servers. If you
        need to delete this setup, deleting it can sometimes be tricky
        due to dependencies, but something like this should work (it
        also worth noting that sometimes it is easier to delete stuff
        using the GUI (Horizon) than the command-line):
        
            # deleting what you just created:
            openstack router remove subnet router1 subnet1
            openstack router delete router1
            openstack subnet delete subnet1
            openstack network delete net1
    
    9.  Boot the most recent LTS Ubuntu release available in glance (the
        image store, `openstack image list`) in your network (`openstack
        network list`) (Remember to use your key, it will be injected
        into the instance by cloud-init, and you will use it to log in
        later). You need to choose a flavor as well. Flavors tell
        OpenStack what resources to allocate to VMs (`openstack flavor
        list`). Generally try to budget as small as possible, you are
        given a limited quota of resources. For this VM, choose at least
        4GB of RAM and 30GB of harddisk space.
        
            openstack server create --image NAME_OR_ID --flavor NAME_OR_ID \
             --nic net-id=UUID --key-name KEY_NAME INSTANCE_NAME
            # old deprecated way:
            nova boot --flavor NAME_OR_ID --image NAME_OR_ID --nic net-id=UUID \
             --key-name KEY_NAME INSTANCE_NAME
    
    10. Boot the most recent Windows server available in glance in your
        network. Assume it will need similar resources to your Ubuntu
        server. Create a Powershell script `Install-Updates.ps1` with
        the following contents:
        
            #ps1
            # Install all updates (and reboot as much as needed)
            Set-ExecutionPolicy RemoteSigned -Force
            Install-PackageProvider nuget -Force
            Install-Module PSWindowsUpdate -Force
            if ( (Get-WUList | Measure-Object).Count -gt 0) {
              Get-WUInstall -Install -AcceptAll -IgnoreReboot
              exit 1003  # 1003 - reboot and run the plugin again on next boot
                         # https://cloudbase-init.readthedocs.io/en/latest/tutorial.html#file-execution
            }
            Add-Content -Path "\installed_updates.log" -Value (Get-Date)  # Writes timestamp to file in root directory (usually C:\)
                                                                          # when script has finished updating
        
        Use cloud-init to run your Powershell script on the first start.
        Scripts are passed to cloud-init via the [user data
        mechanism](https://docs.openstack.org/nova/queens/user/user-data.html).
        To accomplish this, add the `--user-data PATH_TO_SCRIPT`
        parameter when you create the server. Remember to use your key,
        it will be used by cloud-init to inject a random password for
        the user `Admin` which you can retrieve with:
        
            nova get-password INSTANCE_NAME KEY_NAME.pem
    
    11. Create floating ips in the `ntnu-internal` and associate them
        with the Ubuntu and Windows instance.
        
            openstack floating ip create ntnu-internal
            openstack server add floating ip INSTANCE_NAME_OR_ID FLOATING_IP_ADDRESS
            # old deprecated way:
            nova floating-ip-create ntnu-internal
            nova floating-ip-associate INSTANCE_NAME_OR_ID FLOATING_IP_ADDRESS
    
    12. Add a security rule to the `default` security group to allow
        ping.
        
            openstack security group rule create --protocol icmp \
             --remote-ip 0.0.0.0/0 default
            # to list all the rules in this security group:
            openstack security group rule list default
            # old deprecated way:
            nova secgroup-add-rule default icmp -1 -1 0.0.0.0/0
    
    13. Add security rules for http and https also to the `default`
        security group.
        
            openstack security group rule create --protocol tcp \
             --remote-ip 0.0.0.0/0 --dst-port 80 default
            openstack security group rule create --protocol tcp \
             --remote-ip 0.0.0.0/0 --dst-port 443 default
            # old deprecated way:
            nova secgroup-add-rule default tcp 80 80 0.0.0.0/0
            nova secgroup-add-rule default tcp 443 443 0.0.0.0/0
    
    14. Create a security group `linux` with a rule allowing ssh. Add
        this security group to the Ubuntu instance.
        
            openstack security group create linux
            openstack security group rule create --protocol tcp \
             --remote-ip 0.0.0.0/0 --dst-port 22 linux
            openstack server add security group INSTANCE_NAME linux
            # old deprecated way:
            nova secgroup-create linux "Rules for Linux"
            nova secgroup-add-rule linux tcp 22 22 0.0.0.0/0
            nova add-secgroup INSTANCE_NAME linux
    
    15. Create a security group `windows` with a rule allowing rdp. Add
        this security group to the Windows instance.
        
            openstack security group create windows
            openstack security group rule create --protocol tcp \
             --remote-ip 0.0.0.0/0 --dst-port 3389 windows
            openstack server add security group INSTANCE_NAME windows
            # old deprecated way:
            nova secgroup-create windows "Rules for Windows"
            nova secgroup-add-rule windows tcp 3389 3389 0.0.0.0/0
            nova add-secgroup INSTANCE_NAME windows
    
    16. Create two 1GB volumes with display names `linux_vol` and
        `windows_vol` and attach them to the Ubuntu and Windows
        instances, respectively.
        
            openstack volume create --size 1 linux_vol
            openstack volume create --size 1 windows_vol
            openstack server add volume INSTANCE_NAME_OR_ID VOLUME_NAME_OR_ID
            # old deprecated way:
            cinder create --display-name linux_vol 1
            cinder create --display-name windows_vol 1
            nova volume-attach INSTANCE_ID VOLUME_ID
    
    17. Log in to the Ubuntu instance, format, create filesystem and
        mount the new volume on `/myvol` (default brukernavn for Ubuntu
        er ubuntu). These commands need to be executed as root which is
        done like this: `sudo COMMAND` (You can also switch to the root
        user with `sudo su` and execute commands, but this is usually
        discouraged. Exit via the `logout` command or by pressing
        CTRL+D)
        
            cfdisk /dev/vdb
            mkfs.ext4 /dev/vdb1
            mkdir /myvol
            mount /dev/vdb1 /myvol
    
    18. Log in to the Windows instance (username `Admin`), use `Disk
        management` to initialize, create volume, filesystem and mount
        the new volume on `D:\`.
        
            # Find disk number (in PowerShell):
            Get-Disk
            # Initialize
            Initialize-Disk DISK_NUMBER
            
            # Partition and format
            New-Partition -DiskNumber DISK_NUMBER -UseMaximumSize `
             -AssignDriveLetter | Format-Volume
            
            # Unmount (if Drive has letter 'D')
            Get-Volume -Drive D | Get-Partition | 
             Remove-PartitionAccessPath -AccessPath D:\
            
            # Mount
            Get-Disk
            Set-Disk -Number 1 -IsOffline $False
            Set-Disk -Number 1 -IsReadonly $False
            Get-Partition -DiskNumber 1
            Get-Partition -DiskNumber 1 -PartitionNumber 2 | Set-Partition -NewDriveLetter D
            
            # Write commands from all sessions to History file
            cat (Get-PSReadlineOption).HistorySavePath | 
             Out-File D:\"history-$(Get-Date -Format 'dd-MM-yyyy')"
    
    19. Disable SMBv1 on the Windows instance (PowerShell as
        administrator).
        
            # These commands are guaranteed to work on servers:
            Get-WindowsFeature FS-SMB1
            Remove-WindowsFeature FS-SMB1
            
            # These equivalent commands are sure to work on workstations as well:
            Get-WindowsOptionalFeature -Online -FeatureName smb1protocol
            Disable-WindowsOptionalFeature -Online -FeatureName smb1protocol
    
    20. Install nginx on the Ubuntu instance, verify that it is up and
        running by browsing its floating ip.
        
            # Install nginx package
            apt-get install nginx
            
            # You can check if the service was installed and is running like this
            service nginx status
    
    21. Delete everything you have created except your keypair.

# Orchestration

## Why?

##### Why Orchestration?

1.  CLI commands

2.  script CLI commands....scale, missing idempotency, create and delete

3.  declarative language, compose infrastructures as stacks with a
    *Configuration definition file* (generic term for Heat code in our
    case now, any kind of declarative code for defining infrastructure
    elements)

*Provisioning*: everything involved in making an element ready for use

## Orchestration Tools

##### Why Orchestration?

  - OpenStack: Heat (yaml)

  - Hashicorp: Terraform (HashiCorp Configuration Language (HCL))

  - Amazon: Cloudformation (yaml/json)

  - Azure: Azure Resource Manager (json)

  - Google Compute Engine: Cloud Deployment Manager (yaml)

  - ...

##### OpenStack Heat

  - See “Heat’s purpose and vision” at [OpenStack
    Heat](https://docs.openstack.org/heat/latest/)

  - *Orchestration is a key technology in Infrastructure as Code.*

Demo: remove srv2 from `iac_rest.yaml`, then  

    openstack stack update -t iac_top.yaml -e iac_top_env.yaml dcsg1005
    git checkout HEAD -- iac_rest.yaml
    openstack stack update -t iac_top.yaml -e iac_top_env.yaml dcsg1005

## Review questions and problems

1.  Describe the Heat component of OpenStack (in other words: What is
    OpenStack Heat?).

2.  How does a Heat resource retrieve/make use of a value passed as a
    parameter to the template? How does a Heat resource reference
    another Heat resource?

3.  After you have completed this chapters lab tutorial, modify the
    template  
    `servers_in_new_neutron_net.yaml` in such a way that the two servers
    can be different (e.g. an Ubuntu and a Windows instance). Also allow
    for a list of security groups to be added as parameters to each
    server. Delete the Heat stack from the previous exercise and launch
    it again but this time with an Ubuntu and a Windows instance (with
    the linux and windows security groups, respectively).

## Lab tutorials

1.  Do the [basic Heat orchestration
    exercises](https://gitlab.com/erikhje/dcsg1005/blob/master/heat-labs.md).

2.  Create a new network with two Ubuntu instances by using the Heat
    template `servers_in_new_neutron_net.yaml` from (remember to always
    examine code that you reuse, but this is from the repo of the
    OpenStack Heat developers so this is probably good code, and reusing
    good code is a best practice we should enforce)
    [openstack/heat-templates](https://github.com/openstack/heat-templates/blob/master/hot)
    
        openstack stack create -t servers_in_new_neutron_net.yaml \
         -e heat_demo_env.yaml heat_demo
    
    Your environment file `heat_demo_env.yaml` should look something
    like this (the environment file is for enforcing what should be a
    well known principle for you: *separating code and data*)
    
        parameters:
          key_name: KEY_NAME
          image: Ubuntu Server 18.04 LTS (Bionic Beaver) amd64
          flavor: m1.small
          public_net: ntnu-internal
          private_net_name: net1
          private_net_cidr: 192.168.1XY.0/24
          private_net_gateway: 192.168.1XY.1
          private_net_pool_start: 192.168.1XY.200
          private_net_pool_end: 192.168.1XY.250
    
    If you get an error immediately when trying to create a stack you
    probably have a syntax error (e.g. your Yaml file does not have
    correct indentation or similar). If syntax is OK, but the stack
    fails to create e.g. `CREATE_FAILED` message or similar, try
    something like  
    `openstack stack event list heat_demo --nested-depth 3`

# PowerShell

## PowerShell

##### PowerShell

  - See [separate document on
    PowerShell](https://gitlab.com/erikhje/dcsg1005/blob/master/powershell.md)

## Review questions and problems

1.  Which command can you add to the pipeline to see all properties and
    methods of the object?
    
    1.  `Get-ChildItem`
    
    2.  `Get-Feature`
    
    3.  `Get-ItemProperty`
    
    4.  `Get-Member`

2.  How do you create an array in PowerShell?
    
    1.  `$arr = @{}`
    
    2.  `$arr = $()`
    
    3.  `$arr = @()`
    
    4.  `$arr = ${}`

3.  Which module gives you access to the *PSGallery*?

4.  What is *splatting*?

5.  What can you do with `Test-NetConnection` that you cannot do with
    `ping`?

6.  Write a command line which recursively outputs all directories in
    your home directory. It should not list files, only directories.

7.  Using
    [splatting](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_splatting)
    (a technique much used by the textbook author), write a command line
    which recursively outputs all files (not directories, only files)
    in  
    `C:\Windows\System32\LogFiles` and also have the parameter
    ErrorAction set to SilentlyContinue.

8.  Write a command line which recursively outputs all files (not
    directories, only files) in your home directory, sort them by file
    size, and only list the five largest files.

9.  Write a command line which outputs all processes which have the
    property `StartTime` within the last hour.

10. Write a command line which outputs the `Name` and `BasePriority` of
    all processes with a `Name` that contains `svc` or `srv`

11. The command line  
    `Get-NetAdapter | Get-NetIPAddress -AddressFamily IPv4` gives the
    following output
    
        IPAddress         : 192.168.180.196
        InterfaceIndex    : 6
        InterfaceAlias    : tap01ea77a6-43
        AddressFamily     : IPv4
        Type              : Unicast
        PrefixLength      : 24
        PrefixOrigin      : Dhcp
        SuffixOrigin      : Dhcp
        AddressState      : Preferred
        ValidLifetime     : 21:55:38
        PreferredLifetime : 21:55:38
        SkipAsSource      : False
        PolicyStore       : ActiveStore
    
    Write a command line which stores just the IPAddress in a variable
    `$my_ip`

## Lab tutorials

1.  Make sure you have done all the included in-class exercises that you
    find in [separate document on
    PowerShell](https://gitlab.com/erikhje/dcsg1005/blob/master/powershell.md)
    (note: some of the last exercises (remoting and copying) require
    that you have created the windows infrastructure in the next item).

2.  **Create the Windows Infrastructure described in the Textbook**.
    Download or clone the git repo at
    <https://gitlab.com/erikhje/iac-heat-a> and create the stack (do
    this on `login.stud.ntnu.no`):
    
    1.  `git clone https://gitlab.com/erikhje/iac-heat-a.git`
    
    2.  `cd iac-heat-a` and create a file `iac_top_env.yaml` with the
        contents (replace `KEY_NAME`)
        
            parameters:
              key_name: KEY_NAME
    
    3.  create the stack with  
        `openstack stack create -e iac_top_env.yaml -t iac_top.yaml iac`
    
    4.  Wait approximately one hour (everything should be provisioned
        with one hour, but it might take another half an hour before the
        domain is created and all hosts appear joined to the domain),
        then log in with RDP to `cl1`
    
    5.  (If the hosts don’t join the domain, log into `dc1` and do
        `Start-Service puppet` as Admin in PowerShell, then wait another
        half an hour)
    
    Note: you learned in previous week’s lab exercise how to retrieve
    the password for the `Admin` user on Windows-instances. In addition
    you now need the `Domain Administrator` user password, which you can
    find on the Linux-instance `manager` in the file  
    `/etc/puppetlabs/code/shared-hieradata/common.yaml`

3.  **Important information**. If not otherwise stated, then be Domain
    Administrator (`RESKIT\Administrator`) when doing the recipies in
    the textbook, and see [Recipes in Server 2019
    Cookbook](https://github.com/doctordns/PowerShellCookBook2019) for
    easier copy and paste. Sometimes you have to make changes to the
    recipies to match your infrastructure (e.g. change IP-addresses and
    passwords). *Make sure you document the changes you make so you can
    easily redo the recipies in case you have to delete and recreate
    your infrastructure.*

4.  **Installing RSAT tools on Windows 10 and Windows Server 2019**. Do
    this recipe, note the following
    
      - If you are copying and pasting from [Recipe 1.1 - Installing
        RSAT
        Tools.ps1](https://github.com/doctordns/PowerShellCookBook2019/blob/master/Chapter%2001%20-%20Establishing%20a%20PowerShell%20Administrative%20Environment/Recipe%201.1%20-%20Installing%20RSAT%20Tools.ps1)
        there might still be a typo in item 3 (this typo is not in the
        textbook)  
        `$ModulesBefore = Get-Module -ListAvailable` should be  
        `$ModulesBeforeRSAT = Get-Module -ListAvailable` and  
        `"$CountOfModulesBefore modules installed...` should be  
        `"$CountOfModulesBeforeRSAT modules installed...`
    
      - Replace item 5-9 with
        
            # Check if RSAT-components are present
            Get-WindowsCapability -Name RSAT* -Online | 
             Format-Table -Property DisplayName,State
            
            # Install all components not present
            # BEFORE YOU DO THIS, do Stop-Service puppet
            # (to avoid "DSC has requested a reboot" messages)
            Get-WindowsCapability -Online |
             ? {$_.Name -match 'RSAT*' -and $_.State -eq 'NotPresent'} | 
             Add-WindowsCapability -Online
            
            # Check state again
            Get-WindowsCapability -Name RSAT* -Online | 
             Format-Table -Property DisplayName,State
    
      - (RSAT-ADDS is already installed on `dc1`, `dc2`, `srv1` and
        `srv2`).

5.  **Exploring package management**. Do this recipe.

6.  **Exploring PowerShellGet and the PSGallery**. Do this recipe.

7.  **Creating an internal PowerShell repository**. Do this recipe, note
    the following
    
      - In item 6, you see a new statement `@"..."@`
        
            $HS = @"
            Function Get-HelloWorld {'Hello World'}
            Set-Alias GHW Get-HelloWorld
            "@
        
        This is an example of a [Here document/Here
        string](https://en.wikipedia.org/w/index.php?title=Here_document&oldid=936530057#Windows_PowerShell)
        which is a way of creating "instant document/multiline string"
        and store it in a variable.
    
      - After item 8, before item 9, do  
        `New-ModuleManifest @NMHT`

8.  **Establishing a code-signing environment**. Do this recipe.

9.  **Implementing Just Enough Administration**. Do this recipe.
    
    After you have completed this, remove the AD objects you have
    created before you do the labs in Chp 3:
    
        Remove-ADUser JerryG
        Remove-ADGroup RKDnsAdmins
        Remove-ADObject 'OU=IT,DC=Reskit,DC=Org'

10. **New ways to do old things (Chp 2)**. Do this recipe.

11. **Configuring IP addressing (Chp 2)**. Do NOT do this recipe, only
    read it.

12. **Installing and authorizing a DHCP server (Chp 2)**. Do NOT do this
    recipe, only read it.

13. **Configuring DHCP scopes (Chp 2)**. Do NOT do this recipe, only
    read it.

14. **Configuring IP addresses from static to DHCP (Chp 2)**. Do NOT do
    this recipe, only read it.

15. **Configuring DHCP failover and load balancing (Chp 2)**. Do NOT do
    this recipe, only read it.

# Directory Services

## DNS

##### DNS - What is it?

  - DNS is a distributed database with (key,value) pairs

  - DNS provides the following primary services
    
      - name-to-IPaddr mapping (A or AAAA)
    
      - (IPaddr-to-name mapping (PTR))
    
      - aliases (CNAME)
    
      - mail routing (MX)

  - Might also be used for
    
      - Other lookups (SRV records, certificates, etc)  
        *Service Discovery* in Windows domains, e.g. where is login
        server?
    
      - Load distribution
    
      - RBL/SPF (spam prevention)
    
      - Finding authorative information about your network\!

##### DNS History

  - 1971  
    RFC226, HOSTS.TXT (Peggy Karp)

  - 1981  
    RFC799, DNS concepts (David Mills)

  - 1982  
    RFC819, DNS structure (Zaw-Sing Su & Jon Postel)

  - 1983  
    RFC882/883, Hostname lookup, authority and delegation (Paul
    Mockapetris)

  - 1984  
    RFC920, Outline of work to be done and  
    TLDs/TopLevelDomains (Jon Postel)

  - 1985  
    Start of DNS, first name registered (symbolics.com or think.com)

##### DNS History

  - A bunch of RFCs has followed  
    (`http://www.dns.net/dnsrd/rfc`)

  - A huge “war” of companies and academia also followed, the problem
    solved today

  - Names can be registered at one of the “ICANN Accredited Registrars”

  - In Norway: NORID responsible for `.no` (and accredits registrars)
    (<http://www.norid.no>)

### Software

##### DNS software

  - A DNS server is usually divided into *a resolver/cache* and *an
    authorative server*. The following servers are common
    
      - Bind 9
    
      - Microsoft DNS
    
      - [Many
        others...](https://en.wikipedia.org/wiki/Comparison_of_DNS_server_software)

  - To query DNS servers we usually use the `dig` program on Linux and
    the `Resolve-DnsName` cmdlet in PowerShell (or `nslookup` on either
    platform)

<!-- end list -->

  - e.g hostname=`www`, domainname=`ntnu.no`,  
    FQDN/FQHN=`www.ntnu.no`

  - Sometimes in DNS context we have to specify the top node of the DNS
    hierarchy as well (the dot at the end): `www.ntnu.no.`

  - The top level node has a serie of root servers which has information
    about all the TLDs

  - The current TLDs can be found at `  `   
    `http://www.icann.org`

  - Information about the root servers can be found at  
    `http://www.root-servers.org`

  - The thirteen root servers are usually listed in a configuration file
    in the DNS server (e.g `C:\Windows\ System32\dns\cache.dns` in
    Microsoft DNS and `named.root` in Bind) which can be retrieved from
    [ftp.rs.internic.net/domain/named.root](ftp.rs.internic.net/domain/named.root)

##### Domains and Zones

![image](/home/erikhje/office/kurs/secsrv/04-dir/tex/../dns-zones.pdf)

A domain is everything under `.no`, but the `.no` zone excludes all its
subdomains which it has delegated. E.g. `ntnu.no` is in the `.no` domain
but not in the `.no` zone.

##### Our Setup

![image](/home/erikhje/office/kurs/secsrv/04-dir/tex/../our-dns-setup.pdf)

### DNS Query

##### How a Query Works

  - DNS works by passing around *Resource Records (RRs)* through port 53
    (TCP if larger than 512 bytes; else UDP)

  - The most important resource records are
    
      - SOA  
        Start Of Authority
    
      - NS  
        Name Server
    
      - MX  
        Mail eXchanger
    
      - A  
        Address, define the canonical name of an IP address
    
      - CNAME  
        Canonical Name, define an alias
    
      - PTR  
        PoinTer Record, define the reverse mapping (the IP address of a
        fqdn)
    
      - SRV  
        Service record, which hosts and ports have the service

On a Microsoft DNS server, try  
`Get-DnsServerResourceRecord reskit.org`

A resource record (which is what we call the DNS data packet, which is
in most cases carried in a UDP packet) always contains

  - Name  
    e.g. the name we want translated to IP address

  - Type  
    e.g. A

  - Value  
    “the answer”, e.g. the IP address

  - TTL  
    Time-To-Live, the time interval that the resource record may be
    cached before it should be retrieved again from the authorative
    server

A query for a RR can be either *recursive* (“do whatever you can to
resolve this”, this is what a client sends to a resolver) or *iterative*
(“please answer me this without asking anyone else”, this is what a
resolver send to an authorative server). In the DNS data packet there is
a bit called RD which is either one or zero if it is a recursive or an
iterative query respectively.

A host can have multiple names, e.g. in a small organization you might
have a server running smtp and imap services and have the names
`smtp.example.com`, `imap.example.com`, `mail.example.com` and
`mikke.example.com`. `mikke` is probably the original name of the server
(sometimes called the canonical name), while `smtp`, `imap` and `mail`,
are names identifying the service the host offers. They should be
implemented either as additional `A` records or as `CNAME`s.
Implementing them as `A` records is the moft efficient since `CNAME`s
leads to twice as many lookups (a `CNAME` maps to an `A` record, so you
have to look up the `A` record to find the IP address), but `CNAME`s
makes it easier to understand which name is a service name and which is
the canonical name. Separating names into canonical hostname and service
names is a good idea since you might have to move the service to a
different host in the future.

##### Interaction of DNS servers

![image](/home/erikhje/office/kurs/secsrv/04-dir/tex/../dns-resolve.pdf)

Typical sequence is

  - Look in hosts-file (`%SystemRoot%\system32\drivers\etc\hosts` on
    Windows)

  - Look in local config for which resolver to use  
    (`Get-DnsClientServerAddress`)

  - Ask the name resolver a recursive query for `www.wikipedia.org`

  - (1) The name resolver checks to see if it has the `A` record for  
    `www.wikipedia.org`, or `wikipedia.org` or `.org`, it does not so it
    has to contact (send an iterative query to) one of the root servers

  - The root server replies with the NS records of the `.org` zone and
    their corresponding A records (glue records\!)

  - (2) It sends one of the `.org` servers an iterative query, e.g. one
    of the `.org` servers

  - The `.org` authorative server replies with the `NS` records of  
    `wikipedia.org` along with their `A` records (glue records)

  - (3) It then sends an iterative query to one of the `wikipedia.org`
    authoritative servers which answers with the `A` record of
    `www.wikipedia.org`

##### Several Caches Involved

![image](/home/erikhje/office/kurs/secsrv/04-dir/tex/../dns-caches.pdf)

`Get-DnsClientCache`

Note that replies are cached and the TTL (Time To Live) field is
important, its commonly set to 24 hours, meaning that the reply you get
might not be correct if the site have changed its authorative DNS within
the last 24 hours.

### Dynamic DNS

##### Dynamic DNS

  - *What to do with DHCP clients needing a name?*
    
      - Get a new name with every new IP address?
    
      - Always have the same name? Dynamic DNS\!

  - Practical and widely used (Active Directory, dyndns.com, ...)

  - A security problem?
    
      - *clients changing server configuration...*

Dynamic DNS means that a client can notify an authorative DNS server
that it has a new IP address, and the server will update the clients DNS
entry to map the same name to the new IP address. This can also apply to
other kinds of DNS records (e.g. service records (SRV) in active
directory). The fundamental security problem here is the concept of a
client being able to force a configuration change on the server. This
might not be a problem, but this should ring a bell in our security
conscious minds.

### DNS security

##### Security and Privacy

  - Security, read table of contents and browse chapter two of [DNS
    Security Introduction and
    Requirements](https://tools.ietf.org/html/rfc4033)

  - Privacy, read [DNS Privacy - The
    Problem](https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+-+The+Problem)

A key problem with DNS is that it is vulnerable to cache poisoning, with
the most famous vulnerability being probably [Multiple DNS
implementations vulnerable to cache
poisoning](https://www.kb.cert.org/vuls/id/800113/) (where all DNS
servers that did not randomize UDP source port were vulnerable).

## LDAP

##### What is a Directory Service?

  - A simple database *optimized for reads and searches (lookups)*

  - Centralized storage of users, computers, services, printers,
    mailinglists, ...

  - Examples are DNS, NIS, *LDAP*

The ratio between reads:writes in a directory service is typically
1000:1, 10000:1 or even 100000:1.

Like phone-books (180.no or gulesider.no) have entries (records) for
each company or person, and possibly have many levels, e.g. Sporting
goods stores and within sporting goods stores you have might have
entries for general sporting goods, but you might also have
subcategories like tennis stores, golf stores, etc. But in general there
is very frequent searches and reads, and very rarely there are new
writes (new registrations or updates).

##### Lightweight Directory Access Protocol

  - LDAP is a client-server protocol for communication with a directory
    service

  - Defined in RFC4510 - RFC4533 *\!* (most important are
    RFC4511\&RFC4512)

  - LDAP directories follow the X.500 model:
    
      - a tree (a hierarchy) of directory entries (records)
    
      - an entry consist of a set of attributes (fields)
    
      - an attribute has a name and one or more values

  - *LDAP uses TCP on port 389*

Note: the actual data in the directory service can be stored in any kind
of backend, e.g. flat files or an relational database, the requirement
however is that the data have to be accessed (read, searched, updated,
added, modified, etc) according to the LDAP protocol.

(rdn: `uid=erikhje` dn: `uid=erikhje,ou=iik,ou=ie,dc=ntnu,dc=no`)

    dc=no
     |
     +--dc=ntnu (these could be one entry dc=no,dc=ntnu)
         |
         +--ou=ie
             |
             +-ou=iik
                |
                +-uid=erikhje
                |     roomNumber=A117
                |     mobile=93034446
                |     .
                |     .
                |     .
                |
                +-uid=eigilo
                +-uid=erjonz
                  .
                  .
                  .

### DN/RDN

##### (Relative) Distinguished Name

  - *DN = RDN + Parent’s DN*

  - DN is the unique identifier (primary key)

  - RDN is unique identifier only at its own level

  - A DN is typically composed of
    
      - DC  
        Domain Component
    
      - OU  
        Organizational Unit
    
      - CN  
        Common Name

e.g. `CN=ThomasL,OU=IT,DC=reskit,DC=org`

### Protocol

##### Operations

  - StartTLS

  - Bind

  - Search

  - Compare

  - Add (*atomic*)

  - Delete (*atomic*)

  - Modify (*atomic*)

  - Modify DN (move entry) (*atomic*)

  - Abandon

  - Extended operation

  - Unbind

StartTLS and Bind for security, StartTLS is part of LDAP while LDAP over
SSL means that we first establish a SSL session (ldaps using port 636,
this is not much in use, instead one does StartTLS after connecting to
port 389).

Note: prefix notation in search (as opposed to infix and postfix)

DEMO:

    ldapsearch -x -b "ou=people,dc=ntnu,dc=no" -h at.ntnu.no \
    "(&(mail=*frode*)(mobile=*95*))" sn givenName mobile
    
    ldapsearch -x -b "ou=people,dc=ntnu,dc=no" -h at.ntnu.no \
    "(mail=erik.hjelmas@ntnu.no)" sn givenName mobile
    
    echo SGplbG3DpXM= | base64 -d

You can [connect to NTNU LDAP from your email
client](https://innsida.ntnu.no/wiki/-/wiki/English/Configuring+LDAP) to
have automatic address lookups.

### Schema

##### Schema

  - Entries are instance of an object

  - *ObjectClass* is the link to Schema which defines the object

  - From RFC4512 4. Directory Schema: “The schema enables the Directory
    system to, for example:”
    
      - prevent the creation of subordinate entries of the wrong
        object-class (e.g., a country as a subordinate of a person)
    
      - prevent the addition of attribute-types to an entry
        inappropriate to the object-class (e.g., a serial number to a
        person’s entry)
    
      - prevent the addition of an attribute value of a syntax not
        matching that defined for the attribute-type (e.g., a printable
        string to a bit string).

The Active Directory LDAP schema allows for the entries necessary for
Unix/Linux user accounts also.

## Kerberos

![image](/home/erikhje/office/kurs/secsrv/04-dir/tex/../kerberos-stallings.pdf)  
<span>(Stallings, W. *Network Security Essentials, 2nd Ed.*.)</span>

In Greek mythology, Kerberos is a many headed dog, the guardian of the
entrance of Hades

Developed at MIT in the 80’s

Provides a centralized authentication server to authenticate users to
servers and servers to users

Relies on conventional encryption, making no use of public-key
encryption

Provides Single-Sign On

Kerberized applications (apps need to support kerberos)

Two versions: version 4 and 5

Version 4 makes use of DES (not good\!)

Used as the standard authentication and authorization in Active
Directory

Kerberos can have the following issues:

Lifetime associated with the ticket-granting ticket

If too short: repeatedly asked for password

If too long: greater opportunity to replay

The threat is that an opponent will steal the ticket and use it before
it expires

## Active Directory

##### Active Directory

  - LDAP and Kerberos (and (Dynamic) DNS)

  - Forests, (Trees), Domains and OUs: Users and Hosts  
    *Scales to really large organization\!*

![image](/home/erikhje/office/kurs/secsrv/04-dir/tex/../ad-users-and-computers.png)

##### Active Directory

  - OUs are the common level for GPOs: should be structured primarely to
    facilitate administrative delegation, secondary to facilite GPOs

![image](/home/erikhje/office/kurs/secsrv/04-dir/tex/../ad-ou-and-users.pdf)

Remember: *AD is LDAP*, e.g.  
`Get-ADObject` `  -LDAPFilter "(ObjectClass=user)" `

The figure was generated with the following PowerShell code:

    choco install graphviz
    Install-Module PSGraph
    
    # PLOT EVERYTHING IN AD:
    # $distinguishednames = Get-ADObject -Filter * | Select-Object DistinguishedName
    
    # OR BETTER LIMIT A BIT:
    # $distinguishednames = Get-ADObject -Filter * -SearchBase `
    # 'cn=users,dc=reskit,dc=org' | Select-Object DistinguishedName
    
    # OR EVEN BETTER:
    $distinguishednames = Get-ADObject -LDAPFilter `
     "(|(Objectclass=organizationalunit)(ObjectClass=user))" |
     Select-Object DistinguishedName
    
    $graph = foreach ($element in $distinguishednames) {
      $entry = $element.DistinguishedName.Split(",")
      if ($entry.Length -gt 1) {
        $idx = 0
        do {
          "`"$($entry[$idx])`"->`"$($entry[$idx+1])`""
          $idx++
        } while ($idx -lt ($entry.Length-1))
      } else {
        if ($entry) {$entry}
      }
    }
    
    $stringgraph = $graph -join "`n"
    
    Write-Output "strict digraph g { `n $stringgraph `n }" | 
     Export-PSGraph -Destination $env:temp\mysil.pdf -ShowGraph

##### Active Directory

  - Sites are physical (not logical) groupings defined by IP subnets

  - Multi-master replication (pull-based replication), but some
    Operations master roles:  
    <span><http://technet.microsoft.com/en-us/library/cc773108%28v=ws.10%29.aspx></span>

  - AD’s dependency on DNS:  
    <span><http://technet.microsoft.com/en-us/library/cc759550%28v=ws.10%29.aspx></span>

Active Directory mostly has the concept that all domain controllers are
equals, but there are some “operations master roles” that need to be on
a specific hosts, so typically there is a “primary” domain controller
even though this is something different from what was called primary
domain controller in early Windows domains in the 90’s.

##### Active Directory

  - Forest  
    domains share structure, schema, global catalog, trust

  - Domain  
    network-wide id, auth, trust, replicate

  - OU  
    organizational units

  - Container  
    like and OU, but cannot have GPOs

  - Group  
    have users, used for access control (groups have SID, OUs don’t)

  - RootDSE  
    root directory server agent service entry (“root of the directory
    information tree”)

See [Well-known users and groups and built-in
accounts](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/delegating-administration-of-default-containers-and-ous#well-known-users-and-groups-and-built-in-accounts)

The most important part about active directory is how to design the set
and structure of Organizational Units (OUs). The best practice for
designing OUs state that OUs should be structured primarely to
facilitate administrative delegation, secondary to facilite GPOs. In
practice this means that if you have have multiple teams of system
administrator, you should delegate responsibility to those teams by
letting them be administrators for a set of OUs. This is closely related
to the secondary purpose: facilitate GPOs. This means that you should
group users and computers in OUs based on we need should have the same
policies applied to them. E.g. in a University you would have a natural
separation between students, faculty and administrative staff. And the
same for computers: students workstations should be in a separate OU
from faculty laptops.

Sites are simple. The concept of site is relevant when your
infrastructure consists of multiple physical sites where you need to
consider that bandwidth between sites might be an issue. If you have
sites that are far apart (e.g. a branch office in a different part of
the world) you will probably want users at those sites to have some
local services instead of communicating with the main physical site for
all computing services. AD sites are just for this purpose, so that you
e.g. can create a Read-Only Domain Controller (RODC) at a low-bandwidth
site to better serve your users at that site.

Read about forests and domains at [Understanding the Active Directory
Logical
Model](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/understanding-the-active-directory-logical-model)

Read about default containers, OUs, groups and users at [Delegating
Administration of Default Containers and
OUs](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/delegating-administration-of-default-containers-and-ous)

## Group Policy

##### Group Policy

  - Policy settings for Computer and for Users
    
      - Computer  
        applies to all users
    
      - Users  
        only for specific users/user groups

  - Policy settings grouped into Objects (Group Policy Objects - GPOs)

### Policy Settings

##### Available Settings

  - Software settings (installations)

  - Windows settings (login scripts, folder redirection, printers, ...)

  - Administrative templates

*Almost everything is changes to the Windows Registry (HKLM and HKCU)*

Settings are grouped into Software settings, Windows settings and
Administrative templates for Computer and for Users. Some settings does
not make sense to set for specific users (or user groups) so they are
not present in the Users category only in the Computer category,
likewise the other way around. When you set something in the Computer
category it will apply to all users.

Administrative templates for Computer are changes in the HKLM part of
the registry, while Administrative templates for User are changes in
HKCU.

If a computer is not joined to a domain, you can use *Local Group
Policy* on that standalone computer if you want to manage it with the
mechanisms of group policy.

DEMO WITH LOCAL GROUP POLICY: disable task manager after CTRL-ALT-DEL:

    User configuration
     -> Administrative templates
     -> System
     -> CTRL-ALT-DEL options

(Note how this is only available to Users, and not under Computer
configuration.)

##### Options for Each Setting

  - Not configured

  - Enabled

  - Disabled, can mean
    
      - Reverse the setting from a previous level
    
      - Force disabling of an OS default

##### Group Policy in a Domain

  - GP Settings vs GP Preferences
    
      - Settings are enforced
    
      - Preferences can be changed by the user after they have been
        applied

  - Pull-based model
    
      - Ordinary hosts: every 90 minutes (30 min random offset
        (splaytime))
    
      - Domain Controllers: every 5 minutes

The agent pulling is the Group Policy Service activated by the registry
key  
`HKLM\SYSTEM\CurrentControlSet\services\gpsvc`. A Group Policy update
can also be forced by running `Invoke-GPUpdate` (if you want it to
happen immediately you do  
`Invoke-GPUpdate -RandomDelayInMinutes 0`).

DEMO “Force disabling of an OS default” (windows has a firewall on by
default, this is not something turned on by Group Policy, but we are
going to use Group Policy to turn it off):

1.  Remember that GPOs cannot be applied to containers like Users and
    Computers, so let’s move the host CL1 into the IT OU:
    
        # On DC2
        
        $MHT1 = @{
            Identity   = 'CN=CL1,CN=Computers,DC=Reskit,DC=ORG'
            TargetPath = 'OU=IT,DC=Reskit,DC=Org'
        }
        Move-ADObject @MHT1

2.  Creating a Group Policy Object is probably best to do with a GUI. We
    can do it with PowerShell if we know the exact registry settings we
    want to apply, but probably we should take advantage of a GUI in the
    design stage of a GPO:
    
        # On DC2
        
        gpme.msc
        # Create a new GPO called 'MyFWSettings'
        Computer configuration
         -> Administrative templates
         -> Network
         -> Network connections
         -> Windows Defender firewall
         -> Domain profile
         -> Protect all network connections (disable)
        # exit gpme

3.  A good strategy would be to have a repository of all GPOs and
    separate those from the ones we actually apply, just import them
    when needed. In other words, create a GPOs, label them starting with
    ’My’ and let those be our "code" which we could put in a git-repo,
    or at least have version controlled and backed-up somewhere.
    
        # On DC2
        
        # copy the one we created to one that we are going to use
        Copy-GPO -SourceName "MyFWSettings" -TargetName "FWSettings"
        
        # link it to the IT Organizational Unit so it will be applied to CL1
        Get-GPO -Name "FWSettings" | 
         New-GPLink -Target "OU=IT,DC=reskit,DC=org"
        
        # btw we can view all GPOs with
        Get-GPO -All -Domain $env:USERDNSDOMAIN
        
        # Remember also that GPOs are just objects in an AD LDAP tree:
        Get-ADObject -LDAPFilter "(ObjectClass=groupPolicyContainer)" | 
         ForEach-Object {Get-GPO -Id $_.Name}
        
        # And they are just a file structure made available to hosts through a share
        Get-ChildItem C:\Windows\SYSVOL\domain\Policies
        Get-SmbShare

4.  Let’s see how this GPO affects CL1. Keep the Firewall control panel
    (`firewall.cpl`) visible along side PowerShell.
    
        # On CL1
        
        Invoke-GPUpdate -RandomDelayInMinutes 0
        
        # We can view report with
        Get-GPOReport -All -Domain $env:USERDNSDOMAIN -ReportType HTML `
         -Path ".\GPOReport1.html"
        .\GPOReport1.html

(another examples is to turn of Shutdown event tracker on Windows
Servers.)

### Processing Order

##### GPO Processing Order

When a host is joined to a domain:

1.  Local GPO

2.  GPO linked to site (do not do this)

3.  GPO linked to domain

4.  GPO linked to OU (do this)

GPO’s are created, tested, then linked to site, domain or OU.

*Last writer wins\! In other words, OU-GPOs overwrites conflicting GPOs
from previous steps (unless `Enforced` is set).*

Remember that site is a defined IP subnet, typically a geographical
location.

<http://technet.microsoft.com/en-us/library/cc754948%28v=ws.10%29.aspx>:

![image](/home/erikhje/office/kurs/secsrv/04-dir/tex/../inheritance.png)

### Tools

##### Group Policy Tools

  - Local GPO: `gpedit.msc`

  - AD: `gpmc.msc`, `gpme.msc`  

  - PowerShell:  
    `Import-Module GroupPolicy`  
    `Get-Command -Module GroupPolicy`

  - Group Policy Results  
    `Get-GPResultantSetOfPolicy`  
    *Resultant Set of Policy - RSoP*

Remember manual client pull with `Invoke-GPUpdate`

##### Some Important Notes

  - Settings for a set of users or computers, managed by the same
    administrators: *should be in same GPO*

  - Filtering by WMI

  - Enforce settings with a new GPO at the domain level to avoid changes
    by OU-administrators

  - Link order can be manipulated

  - *Max 999 GPOs*

<!-- end list -->

  - GPOs cannot be applied to the default categories Users and Computers

  - Higher level policies (domain) can be set to "enforced", meaning the
    can not be overridden by lower level (OU) (Enforced takes precedence
    over Block policy inheritance)

  - Settings that apply to the same set of users or computers and are
    managed by the same administrators, should be in the same GPO

  - A GPO (not individual policy settings) can be filtered by security
    (e.g. instead of all authenticated users, choose a specific user
    group) or WMI to only apply to a subset of users or computers

  - Use WMI filters only when necessary (exceptions), can be time
    consuming or do not time out at all (long logon times...) (WMI
    filters can be though of as conditional statements)

  - Max 999 GPOs can be applied, if you have more, NONE WILL BE
    APPLIED\!

  - Do not change default domain or domain controller policy, instead
    create a new GPO and set it to Enforce

  - You create Enforce settings at the Domain level to avoid changes by
    OU-administrators

  - You can manipulate the link order of GPOs (e.g. the GPOs linked to
    an OU)

##### Great Book\!

![image](/home/erikhje/office/kurs/secsrv/04-dir/tex/../GPbook.png)

## Review questions and problems

1.  What is included in a DNS resource record? Choose four of these.
    
    1.  Connection speed.
    
    2.  TTL.
    
    3.  Routing.
    
    4.  Name.
    
    5.  Type.
    
    6.  Value.
    
    7.  Fragments.
    
    8.  Port 443.
    
    9.  Congestion information.
    
    10. Root DNS servers.

2.  Which DNS resource record defines an alias to a hostname?
    
    1.  CNAME.
    
    2.  A.
    
    3.  PTR.
    
    4.  NS.

3.  Explain how a DNS lookup works. Use `dig rtfm.mit.edu` as an example
    and assume nothing is cached by any of the involved DNS servers.

4.  Write a command line to query both dc1 and dc2 for the ip address of
    srv2 (nice to check that both DNS servers have this info).
    
      - put the server names dc1 and dc2 in an array
    
      - pipe the array into a `ForEach-Object` loop to query each of the
        servers in sequence

5.  Write a command line (actually two command lines) to query all
    windows hosts if they are part of the domain. This can be done with
    `(Get-ComputerInfo).CsPartofDomain`
    
      - create an array with all the hostnames
    
      - use `Invoke-Command` (powershell remoting)
    
      - do NOT use a pipe (the parameters you can give to
        `Invoke-Command` gives you all the functionality you need)
    
      - does `Invoke-Command` query hosts in sequence or in parallell?

6.  Use the cmdlets `Get-ADForest` and `Get-ADDomain` to find out
    
      - who is the Schema Master?
    
      - who are the Replica Directory Servers?

7.  Write a command line that will output all the objects in your Active
    Directory.
    
      - use `Get-ADObject`
    
      - print output in table-format
    
      - only output DistinguishedName and the ObjectClass

8.  Write a command line that will show a list of all cmdlets starting
    with `Get-` that you have available from the module ActiveDirectory.

9.  In the domain reskit.org, create a new organizational unit
    “Employees”. In the organzational unit “Employees”, create a new
    organizational unit “HR”. After you have created them, first delete
    “HR”, then delete “Employees”.

10. Study the section “Using the Best Practices Analyzer” in chapter 14
    of the textbook (feel free to search the Internet as well). Run a
    best practice analysis of your Active Directory. Show the results of
    the scan, but only show entries where there is a “Problem”, and for
    those entries output in list format the properties Source, Severity,
    Category, Title, Problem, Impact, Resolution.

## Lab tutorials

1.  **DNS (Domain Name System)**. DNS works by sending messages called
    Resource Records in UDP packets. It uses UDP since it needs to be
    lightweight and simple. If a packet is lost, we can just try again.
    However, there are security and privacy concerns  over DNS and
    implementations of DNS over HTTPS (DoH) are being deployed and seems
    to work without too big performance hit . However, another privacy
    concern is introduced with DoH: if everyone starts using [the few
    public providers that offers
    DoH](https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+Public+Resolvers),
    they will be able to see everyones DNS lookups (instead of having
    them spread across many local ISPs).
    
    1.  Log in to CL1 and DC1. Which DNS server(s) do they use? Run this
        cmdlet on both hosts
        
            Get-DnsClientServerAddress
    
    2.  If you try to lookup the hostnames `srv2` and `mon`, which
        domain suffixes are attempted added? Run this cmdlets on both
        hosts
        
            Get-DnsClient | Format-Table `
             -Property InterfaceAlias,InterfaceIndex,ConnectionSpecificSuffix
            Get-DnsClientGlobalSetting
            @('srv2','mon') | Resolve-DnsName
    
    3.  What is in their local cache? Run these cmdlets on both hosts
        
            Get-DnsClientCache
            Clear-DnsClientCache
            Get-DnsClientCache
    
    4.  Do a DNS lookup to find the IP address of `rtfm.mit.edu`
        
            Resolve-DnsName rtfm.mit.edu
        
        The CNAME resource record represents an alias. It means it is a
        host name that maps to another host name. The A resource record
        is what we are really looking for. It contains the host name to
        IP address mapping.
    
    5.  Do a reverse DNS lookup to find the host name of ip address we
        found for `rtfm.mit.edu`
        
            $ip = (Resolve-DnsName rtfm.mit.edu).IP4Address
            $ip
            Resolve-DnsName $ip -Type PTR
        
        The PTR resource record contains the reverse mapping. Note that
        some hosts might have A records but not PTR records, meaning a
        host name to IP address lookup will work, but not the other way
        around.
    
    6.  Skip the internal DNS server and ask two other servers for the
        lookup instead: first one of NTNUs servers, then one of Google’s
        public servers.
        
            Resolve-DnsName -Server 129.241.0.200 rtfm.mit.edu
            Resolve-DnsName -Server 8.8.4.4 rtfm.mit.edu
        
        In the output from the Resolve-DnsName command shown below, make
        sure you understand the TTL (Time To Live) number. Run one of
        the commands above repeatedly and see how the number changes.
        
            Name             Type   TTL   Section    NameHost
            ----             ----   ---   -------    --------
            rtfm.mit.edu     CNAME  1799  Answer     xvm-75.mit.edu
    
    7.  Log in to login.stud.ntnu.no and observe the entire hierarchy of
        servers involved in a fresh DNS lookup (here you will also see
        other resource records: the NS (Name Server) record used to
        delegate responsibility for a domain to another DNS server, and
        the RRSIG which belongs to DNSSEC)
        
            dig @129.241.0.200 +trace rtfm.mit.edu
        
        Notice that some servers (root name servers) are responsible for
        the top level called just `.`, some servers are responsible for
        `edu.` and some servers are responsible for `mit.edu.` and one
        of them (the ones responsible for `mit.edu.`) will return the IP
        address of `rtfm.mit.edu`. *Notice that it is not very often
        that we ask the root name servers since there is caching used at
        all levels (temporary storage for a time indicated by the TTL
        (Time To Live) value).* Do you know what caching is? Ask teacher
        if unsure.

2.  **Testing our domain(s)**. It appears we have two DNS domains.
    
    1.  Let’s see which lookups works. Do this on CL1.
        
            $hostnames = @(
              'dc1',
              'dc2',
              'srv1',
              'srv2',
              'cl1',
              'mon',
              'dir',
              'manager'
            )
            $hostnames | Resolve-DnsName
            $hostnames | ForEach-Object {Resolve-DnsName "$_.reskit.org"}
            $hostnames | ForEach-Object {Resolve-DnsName "$_.node.consul"}
    
    2.  Which DNS server is authorative for the domain`reskit.org` and
        which is authorative for the domain `node.consul`?
        
            $hostnames | ForEach-Object {Resolve-DnsName "$_.node.consul" -Server dc1}
            $hostnames | ForEach-Object {Resolve-DnsName "$_.node.consul" -Server dir}
            $hostnames | ForEach-Object {Resolve-DnsName "$_.reskit.org" -Server dc1}
            $hostnames | ForEach-Object {Resolve-DnsName "$_.reskit.org" -Server dir}
        
        Do you understand the output and have the answer? If not, ask
        teacher (or one of your fellow students) to explain.
    
    3.  Go back to CL1 and let’s see how a Windows Domain uses DNS for
        service discovery (SRV records in DNS)
        
            # Where is the "primary domain controller" (the one holding FSMO roles)?
            Resolve-DNSName _ldap._tcp.pdc._msdcs.reskit.org -Type SRV
            
            # Where are all the domain controllers?
            Resolve-DNSName _ldap._tcp.dc._msdcs.reskit.org -Type SRV
            
            # Where is the global catalog?
            Resolve-DNSName _ldap._tcp.gc._msdcs.reskit.org -Type SRV
            
            # where is the Kerberos server(s)?
            Resolve-DNSName _kerberos._tcp.dc._msdcs.reskit.org -Type SRV

3.  **Installing Active Directory with DNS (Chp 3)**. Jump to item four
    “4. After rebooting, ...” (you do not have to reboot), then do the
    rest of this recipe, note the following:
    
      - In item 3 in the textbook (item 7 in the GitHub-script) remember
        to replace `Pa$$w0rd` with your own domain administrator
        password.
    
      - In item 3 in the textbook (item 7 in the GitHub-script) add
        `-SkipPreChecks` to the last cmdlet:  
        `Install-ADDSDomainController @IHT -Credential $CredRK
        -SkipPreChecks`

4.  **Configuring DNS servers, zones, and resource records (Chp 2)**. Do
    this recipe, note the following
    
      - skip item 3, 4 and 5 (since we do not control DHCP)
    
      - always replace `10.in-addr.arpa` with `180.168.192.in-addr.arpa`
        (since our network is `192.168.180.0/24`)
    
      - in item 9 replace `10.42.42.42` with `192.168.180.42`
    
      - in item 12 replace `10.10.10.42` with `192.168.180.242`
    
      - in item 13 replace `10.10.10.10` with `(Resolve-DnsName
        dc1).IPAddress`
    
    If you would like to have the DNS GUI tool on as well (the one you
    see in the Tools menu of Server Manager), do `Add-WindowsFeature
    -Name RSAT-DNS-Server`

5.  **Creating and managing AD users, groups, and computers**. Do this
    recipe.

6.  **Adding users to AD via a CSV file**. Do this recipe, note the
    following
    
      - Do `Install-Module MlkPwgen`
    
      - in “Getting ready”,
        
          - replace `'` with `"`
        
          - replace `Christmas42` with `$(New-Password -Length 12)` (try
            to always replace default passwords, make it a habit)
        
          - reduce the second column to only be the first character.
            "Initials" in AD only allows up to six characters, we can
            verify this with
            
                $schema =[DirectoryServices.ActiveDirectory.ActiveDirectorySchema]::GetCurrentSchema()
                ($schema.FindClass('user').optionalproperties | ? name -eq initials).rangeupper

7.  **Creating a group policy object**. Do this recipe.

8.  **Reporting on AD users**. Do this recipe.

9.  **Finding expired computers and disabled users in AD**. Do this
    recipe.

# Storage and File Services

## Terminology

##### Terminology

Disk, Partition table, Partition, Volume, File system, ACL, ACE, Share,
Storage Replica, SMB, Share

Today users probably store their data in a cloud storage solution like
OneDrive, Google Drive, Dropbox, iCloud, etc . These storage
technologies can help you if you get attacked by ransomware, but that
depends on your setup and subscription. The best protection against
ransomware is backup. But backups don’t help if they are available
online and can be encrypted by ransomware as well. If you have a
enterprise subscription to a cloud storage solution, it probably
includes something like back-ups 30 days back in time, which should be
sufficient to protect you against ransomware in most situations. If you
don’t use cloud storage, make sure you have *offline* backups that you
can restore data from.

Even though cloud storage is popular, on-premise sharing of files is
still (and will always be) widely used in many situations (typically
where regulations require that you have physical control over your
data). The following is a summary of key concepts we need to know about.

  - Disk  
    (`Get-Disk`) What Windows perceives as a physical disk (SSD or HDD).
    In our case this is what we create in OpenStack (in the OpenStack
    component called “Cinder”) as a “Volume” and attach to a server. In
    other words: inside our server this is seen as a physical disk and
    we treat it as a physical disk, but outside our server (in our
    cloud) this is a volume.

  - Partition table  
    (`Initialize-Disk`) On a new physical disk we create a partition
    table. This is written in the first part of the disk, and is either
    a the old-style “Master Boot Record (MBR)” or the newer (and what we
    typically use today) “GUID Partition Table (GPT)”.

  - Partition  
    (`Get-Partition`) A partition is a part of a disk (it is defined in
    the partition table).

  - Volume  
    (`Get-Volume`) A volume is unfortunately not a term that is clearly
    defined. Sometimes it is the same as a partition, sometimes it is
    multiple partitions joined together, sometimes we talk about a
    physical and a logical volume, etc..., it depends on the context.
    For now, just think of it as either a partition or a group of
    partitions treated as one unit. *A volume is something we can create
    a file system on.* (We can also create a filesystem directly on a
    partition, but we typically have a volume layer between the
    filesystem and the partition).

  - File system  
    (`Get-Volume | Format-Table -Property DriveLetter,FileSystemType`) A
    file system is the data structure we write to a volume (or a
    partition in some cases) that allows us to stores files and
    directories/folders. On Windows the most common file system is NTFS.

  - ACL  
    (`Get-NTFSAccess`) All files and folders have and Access Control
    List (ACL).

  - ACE  
    (`Get-NTFSAccess`) An ACL is a list of Access Control Entries (ACE).
    Each ACE defined a user or group and what permission (read, write,
    execute, append, etc) they have. Each entry can be of type allow or
    deny (deny can be used to exclude a permission that a user otherwise
    would have). ACLs are scanned by the operating system (Windows) in
    order and the first ACE that match the access attempted is used.

Note the paragraph from the textbook:

> In a production environment, it might be appropriate to remove the
> permissions for the Domain Admins account once the users are able to
> access and use the folder successfully. If a Domain Administrator does
> need to change the ACL, they could just take ownership of the folder,
> give themselves full control of the folder, and then perform any
> needed management (and removing any temporary access once this
> maintenance is complete).

  - Storage Replica  
    (`Get-SRPartnership`) This is a Windows Feature that replicates data
    (files and directories/folders) between storage volumes.

  - SMB (Server Message Block)  
    (`Get-SmbShare`) SMB allows you to share files over a network.

  - Share  
    A share is a directory that you make available from a SMB server.

## Review questions and problems

1.  Which sequence makes the most sense?
    
    1.  partition - disk - volume
    
    2.  disk - volume - partition
    
    3.  volume - disk - partition
    
    4.  disk - partition - volume

2.  The old command `net use` is in PowerShell replaced with
    
    1.  `Show-FileShare`
    
    2.  `Get-SmbMapping`
    
    3.  `New-SmbConnection`
    
    4.  `Use-NetConnection`

3.  What is the difference between the cmdlets `Get-NTFSAccess` and
    `Get-SmbShareAccess`?

4.  Write a command line that will output the following (note: sorted by
    size)
    
        DiskNumber PartitionNumber        Size
        ---------- ---------------        ----
                 1               3       66048
                 1               1    16693760
                 2               1    16759808
                 2               2  4294967296
                 2               3  6424625152
                 1               2 10719592448
                 0               1 32210157568

5.  Write a command line that will show all volumes that have more than
    5GB size remaining.

6.  Create a PowerShell script (sequence of command lines) that will
    
    1.  Create a folder `C:\DCdocs`
    
    2.  Remove all access control entries, note the issue
        [Remove-NTFSAccess cannot remove an ACE with the GenericAll
        AccessRights](https://github.com/raandree/NTFSSecurity/issues/17)
    
    3.  The only access control entry should give `RESKIT\Domain Admins`
        `FullControl`

7.  Write a command line that returns ’TRUE’ if both SMB signing and
    encryption is enabled.

8.  Search the Internet and find the simplest command for checking that
    SMB version 1 is not enabled.

## Lab tutorials

1.  **Update your stack with four volumes.**
    
        # On login.stud.ntnu.no
        cd iac-heat-a
        git pull
        # if your stack name is iac: (if not replace iac in the command)
        openstack stack update -t iac_top.yaml -e iac_top_env.yaml iac

2.  **Managing physical disks and disk volumes.** Do this recipe.

3.  **Managing NTFS permissions.** Do this recipe.

4.  **Managing Storage Replica.** Do this recipe, note the following
    
      - If you need to find out which volume is in which disk, do e.g.  
        `Get-Volume F | Get-Partition | Get-Disk`
    
      - In item 4 and 6, add the command  
        `Add-WindowsFeature RSAT-Storage-Replica`
    
      - Before you reach item 3, you need to prepare the new disks on
        srv2 in the same way you did with the disks on srv1, do this on
        srv1:
        
            $SB = {
            
            # Initialize the disks
            Get-Disk | 
              Where PartitionStyle -eq Raw |
                Initialize-Disk -PartitionStyle GPT 
            
            $NVHT1 = @{
              DiskNumber   =  1 
              FriendlyName = 'Storage' 
              FileSystem   = 'NTFS' 
              DriveLetter  = 'F'
            }
            New-Volume @NVHT1
            #  Create two volumes in Disk 2 - first create G:
            New-Partition -DiskNumber 2  -DriveLetter G -Size 4gb
            # Create a second partition H:
            New-Partition -DiskNumber 2  -DriveLetter H -UseMaximumSize
            # Format G: and H:
            $NVHT1 = @{
              DriveLetter        = 'G'
              FileSystem         = 'NTFS' 
              NewFileSystemLabel = 'Log'}
            Format-Volume @NVHT1
            $NVHT2 = @{
              DriveLetter        = 'H'
              FileSystem         = 'NTFS' 
              NewFileSystemLabel = 'GDShow'}
            Format-Volume @NVHT2
            }
            Invoke-Command -ComputerName SRV2 -ScriptBlock $SB
    
      - **This recipe fails in Item 8 when you do `New-SRPartnership
        @SRHT -Verbose`**, here is some context if you want to try and
        figure out the problem:
        
            PS C:\Users\Administrator> New-SRPartnership @SRHT -Verbose
            
            DestinationComputerName : SRV2
            DestinationRGName       : SRV2RG
            Id                      : daf005e4-5ee5-417d-8fec-3f5fadf46c64
            SourceComputerName      : SRV1
            SourceRGName            : SRV1RG
            PSComputerName          :
            
            VERBOSE: Cannot update state for replication group SRV2RG 
             in the Storage Replica driver.
            VERBOSE: Unable to synchronize replication group SRV2RG, 
             detailed reason: Cannot update state for replication group
             SRV2RG in the Storage Replica driver.
            New-SRPartnership: Unable to synchronize replication 
             group SRV2RG, detailed reason: Cannot update state for 
             replication group SRV2RG in the Storage Replica driver.
            
            # To clean up and try again, do
            
            Get-SRPartnership | Remove-SRPartnership
            Get-SRGroup | Remove-SRGroup
            Enter-PSSession srv2
            Get-SRPartnership | Remove-SRPartnership
            Get-SRGroup | Remove-SRGroup
            exit

5.  **Managing Filestore quotas.** Do NOT do this recipe, only read it.

6.  **Using filesystem reporting.** Do NOT do this recipe, only read it.

7.  **Setting up and securing an SMB file server (Chp 5).** Do this
    recipe, BUT DO THIS ON SRV2 INSTEAD OF FS1, and note the following
    
      - If you have installed StorageReplica in the previous lab, you
        will in item 7 get the following error (here with solution):
        
            Restart-Service lanmanserver
            Restart-Service : Cannot stop service 'Server (lanmanserver)' 
             because it has dependent services. It can only be stopped
             if the Force flag is set.
            
            # This is due to StorageReplica:
            
            Get-Service LanmanServer | Format-List -Property DependentServices
             DependentServices : {StorageReplica}
            
            # When we know this, we can use -Force parameter without worry
            
            Restart-Service lanmanserver -Force

8.  **Creating and securing SMB shares.** Do this recipe, BUT DO THIS ON
    SRV2 INSTEAD OF FS1, and note the following
    
      - Before you begin, do
        
            Install-Module NTFSSecurity -Force
            Install-Module MlkPwgen -Force
            
            $OUPath = 'DC=Reskit,DC=Org'
            New-ADOrganizationalUnit -Name Sales -Path $OUPath
            
            # Setup for creating users for sales
            $OUPath = 'OU=Sales,DC=Reskit,DC=Org'
            $Password   = $(New-Password -Length 12)
            $PasswordSS = ConvertTo-SecureString -String $Password `
             -AsPlainText -Force
            $NewUserHT  = @{
              AccountPassword       = $PasswordSS;
              Enabled               = $true;
              PasswordNeverExpires  = $true;
              ChangePasswordAtLogon = $false
              Path                  = $OUPath
            }
            
            # Create Sales users Nigel, Samantha, Pippa, Jeremy
            
            New-ADUser @NewUserHT -SamAccountName Nigel `
             -UserPrincipalName 'Nigel@reskit.org' `
             -Name "Nigel" -DisplayName 'Nigel Hawthorne-Smyth'
            New-ADUser @NewUserHT -SamAccountName Samantha  `
             -UserPrincipalName 'Samantha@reskit.org' `
             -Name "Samantha" -DisplayName 'Saamantha Rhees-Jenkins'
            New-ADUser @NewUserHT -SamAccountName Pippa `
             -UserPrincipalName 'Pippa@reskit.org' `
             -Name "Pippa" -DisplayName 'Pippa van Spergel'
            New-ADUser @NewUserHT -SamAccountName Jeremy `
             -UserPrincipalName 'Jeremy@reskit.org' `
             -Name "Jeremy" -DisplayName 'Jeremy Cadwalender'
            
            # Create Sales Groups
            $OUPath = 'OU=Sales,DC=Reskit,DC=Org'
            New-ADGroup -Name Sales -Path $OUPath -GroupScope Global 
            New-ADGroup -Name SalesAdmins -Path $OUPath -GroupScope Global 
            
            # Add users to the groups
            Add-ADGroupMember -Identity Sales -Members Nigel, Samantha, Pippa, Jeremy
            Add-ADGroupMember -Identity SalesAdmins -Members Nigel, Samantha

9.  **Accessing data on SMB shares.** Do this recipe, note the following
    
      - REPLACE FS1 with SRV2 in this recipe.
    
      - Skip item 8 (since we do not have MarsInstaller.exe).

10. **Creating an iSCSI target.** Do NOT do this recipe.

11. **Using an iSCSI target.** Do NOT do this recipe.

12. **Configuring a DFS Namespace.** Do NOT do this recipe.

13. **Configuring DFS Replication.** Do NOT do this recipe.

# Updates, Patching, Packages

## Introduction

### Software App

##### What Is a Software Application?

  - *Executables*

  - Shared and non-shared libraries

  - Images (icons), sounds

  - Manual/Help files

  - Directories

  - Config files and registry/database entries

  - Menu entries and shortcuts

  - Environment variables

A software application is much more than just the executable file you
run to start the application.

(On Unix/Linux use `ldd` to print shared library dependencies for an
executable).

### Licensing

##### Licensing Issues

  - Open Source/Free software

  - EULA’s

  - Product keys

  - Dual licensing

  - Product activation

  - Time-restricted licenses

  - Per user/Per host licenses

  - License servers

  - *Keeping track of license use ...*

You can make a lot of money just by being a software licensing expert
and doing consultancy services ...

## Updates

##### Updates Fail

![image](/home/erikhje/office/kurs/secsrv/06-updates/tex/../windows-update-problems.png)

##### Fresh Install vs Update

  - Where are all the files to be updated?

  - What if the update fails?

  - *Why are updates different from fresh installs?*
    
      - no physical access required?
    
      - host may not be in a “known state”
    
      - host may have “live” users
    
      - host may be gone
    
      - host may be dual-boot

There has to be a way to locate the files installed from previous
versions of the package, and during an update process everything have to
be carefully backed up so it can be restored if something fails (e.g. a
power shutdown while copying files).

### Uninstall

##### Uninstall and Transactional Behaviour

  - Installations/updates have to be transactional/atomic in behaviour

  - There should also be a transactional uninstall

  - *Should the uninstall remove the dependencies that were installed
    together with the original installation?*

Removing dependencies that were installed together with the original
installation is also sometimes called *cascading package removal*.

    package ->     remote repo                 (  ->  local repo  )   ->  host
               - windowsupdate.microsoft.com         - wsus
               - *.windowsupdate.microsoft.com     ( - chocolatey )
               - *.update.microsoft.com
               - download.microsoft.com
               - dl.delivery.mp.microsoft.com
                 ...

There are many kinds of update packages, search for “Windows update
formats” in [Raymond Chen’s blog “Old New
Thing”](https://devblogs.microsoft.com/oldnewthing/author/oldnewthing)
to learn about full, delta, express and quality updates.

##### SW from Everywhere\!

![image](/home/erikhje/office/kurs/secsrv/06-updates/tex/../update-screenshot.png)

For non-Microsoft software you probably will manage much of it as
chocolatey packages, and if so, you should [set up your internal
repository](https://chocolatey.org/docs/how-to-setup-internal-package-repository)
with the same train of thought as you have with WSUS.

### Security

##### Package Management Security

*Installations are performed with high (root/administrator) privileges:
We really need to trust the packages (and the source they come from) we
are installing\!*

And of course, this means using cryptographic hashes and digital
signatures, hashing and signing maybe repository index metadata (root
metadata), package metadata and the package data itself.

Samuel and Cappos  did an interesting study on security of package
managers back in 2009. These attacks probably does not work anymore, but
how to think about attacks on package management is still useful.

##### Replay Attack

If the attacker is MITM, they can serve an old version of the repository
even though the root metadata is signed ...

*Protect by making sure you dont accept metadata older than what you
already have*.

##### Freeze Attack

If the attacker is MITM, she can avoid updating the repository ...

*Protect by limiting how long signed root metadata is valid*.

##### Metadata Manipulation Attack

If metadata is not signed (not root nor package metadata), MITM can
easily offer newer versions of packages which are actually older version
(with vulnerabilities the attacker know how to exploit) ...

*Protect by requiring signed metadata*.

##### Endless Data (DOS) Attack

As root metadata the MITM attacker will just serve an endless file ...

*Protect by monitoring system resources, setting hard limits or possibly
by keeping package management cache on a separate partition*.

##### Protection

  - Consider using distributions who maintain control over their
    software repositories

  - Maintain your own software repository for your infrastructure

The problem is that its too easy to become a repository maintainer
(mirror a repository) for many distributions, and as soon as you are a
mirror you can initiate the mentioned attacks if the distribution is
vulnerable.

## Practice

### PowerShell

##### PackageManagement

PowerShell’s package management architecture

![image](/home/erikhje/office/kurs/secsrv/06-updates/tex/../OneGetArchitecture.png)  
<span> from [PackageManagement (aka
OneGet)](https://github.com/oneget/oneget)</span>

    PS> Get-Command -Module PackageManagement |
         Format-Table -Property Name
    
    Name
    ----
    Find-Package
    Find-PackageProvider
    Get-Package
    Get-PackageProvider
    Get-PackageSource
    Import-PackageProvider
    Install-Package
    Install-PackageProvider
    Register-PackageSource
    Save-Package
    Set-PackageSource
    Uninstall-Package
    Unregister-PackageSource

But unified package management is not easy, e.g updates are still
problematic with PowerShell’s high level framework. But it is a good
reference to understand the different packages and sources we have to
deal with on Windows. To try to get an overview we can list installed
packages in different ways and compare:

    PS> Get-CimInstance Win32_Product | 
         Format-Table -Property Name,Version
    
    Name                         Version
    ----                         -------
    Sensu Agent                  5.17.1.9246
    Puppet Agent (64-bit)        6.13.0
    Cloudbase-Init 0.9.12.dev125 0.9.12.0
    
    
    PS> choco list --local-only
    
    Chocolatey v0.10.15
    7zip 19.0
    7zip.install 19.0
    chocolatey 0.10.15
    chocolatey-core.extension 1.3.5.1
    DotNet4.5.2 4.5.2.20140902
    git 2.25.1
    git.install 2.25.1
    less 5.51.0.20191024
    sensu-agent 5.17.1.9246
    vscode 1.42.1
    vscode.install 1.42.1
    11 packages installed.
    
    
    # From
    # https://gallery.technet.microsoft.com/scriptcenter/Get-RemoteProgram-Get-list-de9fd2b4
    # Same as what you see in the Apps GUI (basically reads the registry)
    PS> Get-RemoteProgram
    
    ComputerName ProgramName
    ------------ -----------
    SRV2         7-Zip 19.00 (x64)
    SRV2         Git version 2.25.1
    SRV2         Sensu Agent
    SRV2         Puppet Agent (64-bit)
    SRV2         Microsoft Visual Studio Code
    SRV2         Cloudbase-Init 0.9.12.dev125
    
    
    PS> Get-Package 
         | Format-Table -Property Name,Version,Providername
    
    Name                                                  Version     ProviderName
    ----                                                  -------     ------------
    Sensu Agent                                           5.17.1.9246 msi
    Puppet Agent (64-bit)                                 6.13.0      msi
    Cloudbase-Init 0.9.12.dev125                          0.9.12.0    msi
    7-Zip 19.00 (x64)                                     19.00       Programs
    Git version 2.25.1                                    2.25.1      Programs
    Microsoft Visual Studio Code                          1.42.1      Programs
    Update .. Antivirus - KB2267602 (Version 1.311.679.0)             msu
    Update .. Antivirus - KB2267602 (Version 1.311.635.0)             msu
    Update .. Antivirus - KB2267602 (Version 1.311.611.0)             msu
    Update .. Antivirus - KB2267602 (Version 1.311.598.0)             msu
    Update .. antimalware - KB4052623 (Version 4.18.2001.10)          msu
    Update .. Antivirus - KB2267602 (Version 1.311.539.0)             msu
    Update .. Antivirus - KB2267602 (Version 1.311.535.0)             msu
    Update .. Antivirus - KB2267602 (Version 1.311.528.0)             msu
    Update .. Antivirus - KB2267602 (Version 1.311.532.0)             msu

### WSUS

##### Windows Server Update Services

  - `Get-WsusUpdate`

  - `Get-WsusProduct`

  - `Get-WsusClassification`

  - `$WSUSServer.GetSubscription()`

  - `$WSUSSub.GetSynchronizationStatus()`

  - `$WSUSServer.GetComputerTargetGroups()`

  - `$WSUSServer.GetInstallApprovalRules()`

  - `Get-WsusComputer`

With WSUS, you don’t get access to everything you might need from the
cmdlets, so many times you will use the methods of the wsuserver-object
instead of cmdlets, e.g.

    $WSUSServer = Get-WsusServer
    $WSUSServer.GetConfiguration()

The downside to this is that these methods are not as well documented as
cmdlets.

There is another good PowerShell module for WSUS at
<https://github.com/proxb/PoshWSUS>

## Review questions and problems

1.  By default, Windows computers, both the server and client version,
    download updates from Microsoft’s Windows Update servers on the
    internet. Which mechanism do you typically use to configure them to
    use your WSUS server instead?
    
    1.  PowerShell remoting
    
    2.  Group Policy
    
    3.  SSH
    
    4.  Splatting

2.  What does it mean that the method StartSynchronization() is an
    asynchronous operation?

3.  In the command line  
    `Test-Path -Path $WSUSDir -ErrorAction SilentlyContinue`  
    What is the purpose of `-ErrorAction SilentlyContinue`?

4.  Write a command line that will output only the
    `LastSynchronizationTime` of the WSUS server.

5.  Write a command line that will show information about LocalPort 8530
    and 8531.

6.  Write a command line that will list all updates that are
    `Unapproved` and have a title that matches `x64`

7.  Write a command line that will count the number of lines in the WSUS
    log file.

## Lab tutorials

1.  **Installing Windows Update Services** Do this recipe, REPLACE WSUS1
    WITH SRV2, and note the following
    
      - In item 4 you have to wait for it to complete before you move on
    
      - BEFORE ITEM 13 DO THIS (btw it hurts to do this with a GUI but
        after five hours I cannot find a way of doing this on the
        command line, which means its much harder to document, we cant
        automate and version control)
        
        1.  Start the WSUS GUI tool you find in Server Manager, Tools,
            Windows Server Update Services (if you do not immediately
            enter the WSUS Server Configuration Wizard, you will find it
            by clicking SRV2, Options)
        
        2.  Click next (do not join the Microsoft Improvement Program)
            until you reach “Download update information from Microsoft
            Update”, click “Start connecting” (this takes a few minutes)
        
        3.  Choose only English as language
        
        4.  Do NOT choose “All products”
        
        5.  Do NOT choose “Windows”
        
        6.  Only choose the three products with names containing
            “Windows 10 version 1903” and the three products with
            names containing “Windows Server 2019”
        
        7.  Leave classification as is
        
        8.  Choose “Synchronize manually”
        
        9.  Do NOT choose “Begin initial synchronization”
        
        When you now do item 13 it should take about half an hour
        (instead of 13 hours which is how long it would take if you only
        followed the recipe as is). If you start the sync that takes 13
        hours, don’t worry, it’s a async (background) process that can
        keep running while you do other things.

2.  **Configuring WSUS update synchronization.** Do this recipe, note
    the following
    
      - Replace item 3 with
        
            $CHP = (Get-WsusProduct |  
              Where-Object -FilterScript {$_.product.title -match 
                '^(Windows Server 2019.*|Windows 10, version 1903|Active Directory)'}).Product.Title
    
      - Item 10 and 11 is not very robust, behaviour depends a bit on
        how much you synchronize, you can either skip to item 12 or
        replace `While` with `Until`
    
      - In item 11 there is a typo, replace `$NP = 'NotProessing'` with
        `$NP = 'NotProcessing'`

3.  **Configuring the Windows Update Client.** Do this recipe, REPLACE
    WSUS1 WITH SRV2, note the following
    
      - If cmdlets like `Get-WsusServer` is not available do
        
            Get-WindowsCapability -Online |
              Where-Object {$_.Name -match 'RSAT.wsus*'} | 
                Add-WindowsCapability -Online
    
      - In item 4, if you get an error check that the directory `C:\foo`
        exists, if it doesn’t do
        
            New-Item -Type Directory c:\foo

4.  **Creating computer target groups.** Do this recipe, REPLACE WSUS1
    WITH SRV2.

5.  **Configuring WSUS automatic approvals.** Do this recipe, note the
    following
    
      - In item 3 you might have to replace `Critical Updates` with
        `Kritisk Oppdatering`
    
      - In item 3 you might have to replace `Definition Updates` with
        `Definisjonsoppdateringer`

6.  **Configuring WSUS automatic approvals.** Do this recipe, note the
    following
    
      - In item 3 replace `Windows Server 2016` with `Windows
        Server 2019`
    
      - In item 3 replace `Security Updates` with
        `Sikkerhetsoppdatering`
    
      - In item 5 replace `3194798` with `4470502` (same in item 9
        replace `4020821` with `4483235`) or another number you find
        with
        
            $SecurityUpdates | Sort-Object -Property Title |
              Select-Object -First 10 |
                Format-Table -Property Knowledgebasearticles

# Web Services and Certificates

## Terminology

  - Internet Information Services (IIS)  
    is Microsoft’s web server. Probably most widely used for Microsoft
    web application on internal networks (or for web applications that
    require a Microsoft stack in general). Large scale public facing
    internet services use open source alternatives like Apache or nginx.

  - Port 80 and 443  
    ordinary http is in port 80 while https is on port 443, but note
    that they can be on any port, and many other services besides www
    use http(s) as their protocol.

  - Site  
    (`Get-WebSite` or `Get-ChildItem IIS:\Sites\`) A webserver can have
    many sites.

  - SSL/TLS  
    A site or server typically have one certificate (it can have
    multiple certificates). A Certificate is a public key (recall RSA
    from the mathematics-course) wrapped with some meta-information
    (domainname, expiration date, etc) and the public key is signed by a
    third party. If the third party is you, meaning you sign it with its
    own private key, it’s called a self-signed certificate. A
    self-signed certificate is only used for testing SSL/TLS. Btw, SSL
    and TLS is by all practical means the same thing (TLS is the new
    version of SSL, but everyone still calls it SSL).

  - Cipher (suite)  
    A cipher is a cryptographic algorithm like RSA, and suite means all
    versions of that algorithm that the server accepts.

  - Binding  
    (`Get-WebBinding`) A binding directs a request to the correct
    website. Maps an IP/Port to a site name.

  - Server Name Indication (SNI)  
    An extension to SSL/TLS where a client can indicate which hostname
    it is attempting to connect to at the start of SSL connection.
    Allows for separate certificates for each site/domain.

## Under the Hood

From Ashraf Khan. “Microsoft IIS 10.0 Cookbook” Packt publishing, 2017.

![image](/home/erikhje/office/kurs/secsrv/07-wwwssl/tex/../iis-flow.png)

The Windows operating system process `system` is listening on port 80
and 443 using its kernel driver `http.sys`

    Get-NetTCPConnection -LocalPort 80,443 | 
     Select-Object LocalPort, 
      @{name='ProcessName';expression={(Get-Process -id $_.OwningProcess).Name}}
    
    Get-ChildItem \windows\system32\drivers\http.sys
    
    HKLM:\SYSTEM\CurrentControlSet\Services\HTTP\Parameters\

http.sys passes requests on to contacts `was` to ask to initiate the web
service `w3svc` with config from `applicationHost.config`

`w3svc` talks to http.sys and asks a worker process to send requested
web page to http.sys which sends it to the requesting host

    # was and w3svc are DLLs loaded in svchost processes
    
    Get-CimInstance Win32_Service  | 
     Where-Object {$_.Started -eq "True" -and $_.ServiceType -eq "Share Process"} | 
      Select-Object `
       @{name='ProcessName';expression={(Get-Process -id $_.ProcessID).Name}}, 
        Name, ProcessId
    
    # w3svc depend on was to initiate, was does not depend on w3svc
    
    (Get-Service was).DependentServices
    
    (Get-Service w3svc).DependentServices
    
    # use 'grep' with config file:
    Select-String binding C:\Windows\System32\inetsrv\Config\applicationHost.config

## Review questions and problems

1.  Sometimes it is nice to check which certificate is used by a website
    on our server. Study the following PowerShell-session:
    
        PS> Get-ChildItem IIS:\SslBindings | 
            Where-Object {$_.Port -eq 443} | Select-Object *
        
        IPAddress                     : 0.0.0.0
        Port                          : 443
        Host                          :
        Thumbprint                    : 4E95069965F57EE283C42E8B703C795D180353B5
        Store                         : MY
        ApplicationId                 : 4dc3e181-e14b-4a21-b022-59fc669b0914
        RevocationFreshnessTime       : 00:00:00
        RevocationURLRetrievalTimeout : 00:00:00
        CTLIdentifier                 :
        CTLStoreName                  :
        CertificateCheckMode          : 0
        DefaultFlags                  : 0
        PSPath                        : WebAdministration::\\SRV1\SslBindings\0.0.0.0!443
        PSParentPath                  : WebAdministration::\\SRV1\SslBindings
        PSChildName                   : 0.0.0.0!443
        PSDrive                       : IIS
        PSProvider                    : WebAdministration
        PSIsContainer                 : False
        Sites                         : Microsoft.IIs.PowerShell.Framework.ConfigurationAttribute
        
        
        PS> Get-ChildItem Cert:\LocalMachine\My\4E95069965F57EE283C42E8B703C795D180353B5
        
           PSParentPath: Microsoft.PowerShell.Security\Certificate::LocalMachine\My
        
        Thumbprint                                Subject
        ----------                                -------
        4E95069965F57EE283C42E8B703C795D180353B5  CN=SRV1.Reskit.Org
    
    Write two command lines where the first one stores the object from
    the first command line above in a variable `$site` and the second
    one outputs only `CN=SRV1.Reskit.Org` (in other words, avoid the
    hard-coding of `My` and  
    `4E95069965F57EE283C42E8B703C795D180353B5`)

## Lab tutorials

1.  **Installing IIS.** Do this recipe, note the following
    
      - Before you start (and do this whenever you are wondering if a
        web server is running), check if any service is listening on
        port 80 or 443 with  
        `Get-NetTCPConnection -LocalPort 80,443`
    
      - In item 1, replace the `$FHT` block with
        
            $FHT = @{
              Name = 'Web-Common-Http','Web-Health','Web-Security'
              IncludeAllSubFeature   = $true
              IncludeManagementTools = $true
            }

2.  **Configuring IIS for SSL.** Do this recipe.

3.  **Managing TLS cipher suites.** Do this recipe, note the following
    
      - in item 6, if you are copying from GitHub, replace `ggeHash`
        with `Hash`

4.  **Configuring a central certificate store.** Do this recipe, note
    the following
    
      - In item 6, do `Install-Module MlkPwgen -Force` then replace  
        `$Certpw = 'SSLCerts101!'` with  
        `$Certpw = New-Password -Length 16`  
    
      - In item 9, replace `$Password = 'Pa$$w0rd'` with  
        `$Password = New-Password -Length 12`  
        (default passwords are our enemies)
    
      - In item 10, do `New-Item -Path $IPHT.Path -Force`  
        before you do `Set-ItemProperty @IPHT`

5.  **Configuring IIS bindings.** Do this recipe, note the following
    
      - In item 2 note the use of a "Here document"/"Here string"
        (`@'...'@`)
    
      - In item 4 replace `'10.10.10.50'` with the output you get from
        the command  
        `(Get-NetAdapter | Get-NetIPAddress).IPv4Address`
    
      - Remember that you can use the cmdlet `Get-WebSite` to get an
        overview of the websites that are present

6.  **Managing IIS logging and log files.** Do this recipe, note the
    following
    
      - In item 8, if you are copying from GitHub, there might be a
        vertical bar (`|`) missing at the end of  
        `Where-Object LastWriteTime -lt $DaysOld`  
        (it is not missing in the textbook)

7.  **Managing IIS applications and application pools.** Do this recipe.

8.  **Analyzing IIS log files.** Do this recipe.

# Logging and Monitoring

## My Section

##### My Frame

  - *Alert* maybe

Some notes....

## Review questions and problems

1.  **Dummy**.

2.  **Dummy**.

3.  **Dummy**.

## Lab tutorials

1.  **Dummy**.
    
        https://download.microsoft.com/download/1/0/5/1059800B-F375-451C-B37E-758FFC7C8C8B/WindowsAdminCenter1910.msi
        (new-object
        System.Net.WebClient).DownloadFile("https://download.microsoft.com/download/1/0/5/1059800B-F375-451C-B37E-758FFC7C8C8B/WindowsAdminCenter1910.msi",
        '.\wac1910.msi')
        
        start med Chrome
        
        +
        søk med AD, bare * gir alle de fire 2019'ene
        merk all og Add
        clik på en for å få full kul oversikt
        
        events og performance monitor
